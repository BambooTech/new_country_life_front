<!doctype html>
<html class="no-js" lang="">
    
    <?php include 'common/head.php'; ?>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="wrapper" class="home">

            <?php include 'common/header.php'; ?>


            <div class="container">
                <div class="row">
                    
                    <?php include 'common/full-sidebar.php'; ?>

                    <div class="col-xs-12 col-sm-6">
                        <section id="fullwidth-products-cart">
                            <div class="row">
                                <div class="col-xs-12 section-title">
                                    <p>
                                        Usted tiene: 3 productos en el carrito
                                    </p>
                                </div>
                                <div class="col-xs-12">
                                    <section id="fullwidth-text">
                                        <p>
                                            <i>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                            </i>
                                        </p>
                                    </section>
                                </div>
                                <div class="col-xs-12 cart-item">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4>
                                                Product name
                                            </h4>
                                            <div class="inline-content">
                                                <div class="inline-container">
                                                    <span class="product-price">
                                                        $ 1244
                                                    </span>
                                                </div>
                                                <div class="inline-container">
                                                    <p class="controls">
                                                        Cantidad: <span class="quantity">2</span>
                                                    </p>
                                                </div>
                                                <div class="inline-container">
                                                    <p class="total-price">
                                                        $999
                                                    </p>
                                                </div>
                                            </div>
                                            <p class="size">
                                                Talle/medida: L
                                            </p>
                                            <p class="color">
                                                Color: <span class="selected-color">&nbsp;</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 cart-item">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4>
                                                Product name
                                            </h4>
                                            <div class="inline-content">
                                                <div class="inline-container">
                                                    <span class="product-price">
                                                        $ 1244
                                                    </span>
                                                </div>
                                                <div class="inline-container">
                                                    <p class="controls">
                                                        Cantidad: <span class="quantity">2</span>
                                                    </p>
                                                </div>
                                                <div class="inline-container">
                                                    <p class="total-price">
                                                        $999
                                                    </p>
                                                </div>
                                            </div>
                                            <p class="size">
                                                Talle/medida: L
                                            </p>
                                            <p class="color">
                                                Color: <span class="selected-color">&nbsp;</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 cart-item">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4>
                                                Product name
                                            </h4>
                                            <div class="inline-content">
                                                <div class="inline-container">
                                                    <span class="product-price">
                                                        $ 1244
                                                    </span>
                                                </div>
                                                <div class="inline-container">
                                                    <p class="controls">
                                                        Cantidad: <span class="quantity">2</span>
                                                    </p>
                                                </div>
                                                <div class="inline-container">
                                                    <p class="total-price">
                                                        $999
                                                    </p>
                                                </div>
                                            </div>
                                            <p class="size">
                                                Talle/medida: L
                                            </p>
                                            <p class="color">
                                                Color: <span class="selected-color">&nbsp;</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <section id="fullwidth-text">
                                        <p>
                                            <i>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                            </i>
                                        </p>
                                    </section>
                                </div>
                            </div>
                        </section>
                    </div>

                    <div class="col-xs-12 col-sm-3">
                        <?php include 'common/resume-confirm.php'; ?>
                    </div>
                </div>
            </div>

            <?php include 'common/footer.php'; ?>
        </div>
    </body>
</html>
