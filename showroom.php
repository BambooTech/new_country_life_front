<!doctype html>
<html class="no-js" lang="">
    
    <?php include 'common/head.php'; ?>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="wrapper" class="home">

            <?php include 'common/header.php'; ?>


            <div class="container">
                <div class="row">
                    
                    <?php include 'common/full-sidebar.php'; ?>

                    <div class="col-xs-12 col-sm-9">
                        <section id="fullwidth-content">
                            <div class="row">
                                <div class="col-xs-12 section-title">
                                    <p>
                                        Título del showroom
                                    </p>
                                </div>
                                <div class="col-xs-12">
                                    
                                    <?php include 'common/section-carousel.php'; ?>

                                    <p>
                                        <span>NEW COUNTRY LIFE</span> ha diseñado un concepto integrado de empresa joven y a la vez con una gran experiencia. Nuestra misión: “Ser los mas confiables, tanto para nuestros proveedores de Brasil, como para nuestros clientes en el mercado Argentino
                                    </p>
                                </div>
                            </div>
                        </section>

                        <section id="twocol-form">
                            <div class="row">
                                <div class="col-xs-12 section-title text-center">
                                    <p>
                                        Solicitar visita del Showroom
                                    </p>
                                </div>
                                <form>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nombre y Apellido</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Razón Social</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Dirección</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Localidad</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Provincia</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Teléfono</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 text-right">
                                          <button type="submit" class="btn btn-green">Enviar</button>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                </div>
            </div>

            <?php include 'common/footer.php'; ?>
        </div>
    </body>
</html>
