<!doctype html>
<html class="no-js" lang="">
    
    <?php include 'common/head.php'; ?>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="wrapper" class="home">

            <?php include 'common/header.php'; ?>


            <div class="container">
                <div class="row">
                    
                    <?php include 'common/full-sidebar.php'; ?>

                    <div class="col-xs-12 col-sm-6">
                        <section id="fullwidth-products-cart">
                            <div class="row">
                                <div class="col-xs-12 section-title">
                                    <p>
                                        Productos destacados
                                    </p>
                                </div>
                                <div class="col-xs-12 cart-item">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <img src="img/demos/checkout1.jpg" class="img-responsive">
                                        </div>
                                        <div class="col-xs-8">
                                            <h4>
                                                Product name
                                            </h4>
                                            <span class="product-price">
                                                $ 1244
                                            </span>
                                            <p class="controls">
                                                Cantidad <span class="quantity">2</span> <a href="#">modificar</a>
                                                <br>
                                                <a href="#">eliminar <i class="fa fa-times" aria-hidden="true"></i></a>
                                            </p>
                                            <p class="size">
                                                Talle/medida: L
                                            </p>
                                            <p class="color">
                                                Color: <span class="selected-color">&nbsp;</span>
                                            </p>
                                            <p class="total-price">
                                                $999
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 cart-item">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <img src="img/demos/checkout2.jpg" class="img-responsive">
                                        </div>
                                        <div class="col-xs-8">
                                            <h4>
                                                Product name
                                            </h4>
                                            <span class="product-price">
                                                $ 1244
                                            </span>
                                            <p class="controls">
                                                Cantidad <span class="quantity">2</span> <a href="#">modificar</a>
                                                <br>
                                                <a href="#">eliminar <i class="fa fa-times" aria-hidden="true"></i></a>
                                            </p>
                                            <p class="size">
                                                Talle/medida: L
                                            </p>
                                            <p class="color">
                                                Color: <span class="selected-color">&nbsp;</span>
                                            </p>
                                            <p class="total-price">
                                                $999
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 cart-item">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <img src="img/demos/checkout1.jpg" class="img-responsive">
                                        </div>
                                        <div class="col-xs-8">
                                            <h4>
                                                Product name
                                            </h4>
                                            <span class="product-price">
                                                $ 1244
                                            </span>
                                            <p class="controls">
                                                Cantidad <span class="quantity">2</span> <a href="#">modificar</a>
                                                <br>
                                                <a href="#">eliminar <i class="fa fa-times" aria-hidden="true"></i></a>
                                            </p>
                                            <p class="size">
                                                Talle/medida: L
                                            </p>
                                            <p class="color">
                                                Color: <span class="selected-color">&nbsp;</span>
                                            </p>
                                            <p class="total-price">
                                                $999
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    <div class="col-xs-12 col-sm-3">
                        <?php include 'common/resume-confirm.php'; ?>
                    </div>
                </div>
            </div>

            <?php include 'common/footer.php'; ?>
        </div>
    </body>
</html>
