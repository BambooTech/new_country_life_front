<!doctype html>
<html class="no-js" lang="">
    
    <?php include 'common/head.php'; ?>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="wrapper" class="home">

            <?php include 'common/header.php'; ?>


            <div class="container">
                <div class="row">
                    
                    <?php include 'common/full-sidebar.php'; ?>

                    <div class="col-xs-12 col-sm-9">
                        <section id="fullwidth-text">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <p>
                                        Gracias por registrarse, le enviaremos a su casilla de email los datos de usurio y contraseña para poder ingresar al sitio y realizar pedidos.
                                    </p>
                                    <p>
                                        Gracias
                                    </p>
                                    <p>
                                        <strong>New Country Life</strong>
                                    </p>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>

            <?php include 'common/footer.php'; ?>
        </div>
    </body>
</html>
