<!doctype html>
<html class="no-js" lang="">
    
    <?php include 'common/head.php'; ?>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="wrapper" class="home">

            <?php include 'common/header.php'; ?>


            <div class="container">
                <div class="row">
                    
                    <?php include 'common/full-sidebar.php'; ?>

                    <div class="col-xs-12 col-sm-9">
                        <section id="fullwidth-product-show">
                            <div class="row">
                                <div class="col-xs-12 section-title">
                                    <p>
                                        Product name
                                    </p>
                                </div>
                                <div class="col-xs-12 col-sm-6 fulwidth-product-main-img">
                                    <a href="#">
                                        <img src="img/demos/product-desc.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4">
                                            <div class="fullwidth-product-gallery">
                                                <a href="#">
                                                    <img src="img/demos/product-thumb1.jpg" class="img-responsive">
                                                </a>
                                                <a href="#">
                                                    <img src="img/demos/product-thumb2.jpg" class="img-responsive">
                                                </a>
                                                <a href="#">
                                                    <img src="img/demos/product-thumb3.jpg" class="img-responsive">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-8">
                                            <div class="fullwidth-product-info">
                                                <h5>
                                                    Descripción del Producto
                                                </h5>
                                                <a href="#">
                                                    <img src="img/demos/slidertouch1.jpg" class="img-responsive">
                                                </a>
                                                <p class="fullwidth-product-detail">
                                                    Tela: 100% Algodón - 330 g/m²
                                                    <br>
                                                    Unidades de venta: 4   
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section id="fullwidth-product-preferences-cloth">
                            <form>
                                <div class="row text-center">
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Talle</label>
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Color</label>
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                                                selected color
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    ...
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Cantidad</label>
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Valor Unitario</label>
                                            <p>$123</p> <small>iva no incluido</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 text-center">
                                    <button type="submit" class="btn btn-default">cargar otra selección</button>
                                    <button type="submit" class="btn btn-green">Agregar al Carrito</button>
                                </div>
                            </form>
                        </section>

                        <section id="fullwidth-product-preferences-cloth">
                            <form>
                                <div class="row text-center">
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Con o sin bolsa</label>
                                            <input type="checkbox">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Color</label>
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                                                selected color
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    ...
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Cantidad</label>
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Valor Unitario</label>
                                            <p>$123</p> <small>iva no incluido</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 text-center">
                                    <button type="submit" class="btn btn-default">cargar otra selección</button>
                                    <button type="submit" class="btn btn-green">Agregar al Carrito</button>
                                </div>
                            </form>
                        </section>

                        <?php include 'common/section-grid.php'; ?>

                    </div>
                </div>
            </div>

            <?php include 'common/footer.php'; ?>
        </div>
    </body>
</html>
