$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        themeClass: 'owl-theme',
        navText:['<i class="fa fa-chevron-left" aria-hidden="true"></i>','<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        loop:true,
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:3,
                nav:false
            },
            1000:{
                items:4,
                nav:true,
                loop:false
            }
        }
    });

    $('.subcategory').hide();

    $('.category-group').click(function(){
        $(this).find('.subcategory').toggle( "fast", function() {

        });

        if($(this).find('.product-name').hasClass('active')){
            $(this).find('.product-name').removeClass('active');
        } else{
            $(this).find('.product-name').addClass('active');
        };
    });

    if ($(window).width() < 1200){
       $('#toggable-sidebar').addClass('mobile-navigation oculto');
       $('.hide-menu').hide();
    } else{
        $('#toggable-sidebar').removeClass('mobile-navigation');
        $('.show-menu').hide();
        $('.hide-menu').hide();
    }

    $('.show-menu').click(function(){
        $('#toggable-sidebar').removeClass( "oculto", function() {
        });
        $('#toggable-sidebar').addClass( "scrol", function() {
        });
        $(this).hide();
        $('.hide-menu').show('fast');
    });

    $('.hide-menu').click(function(){
        $(this).parent('#toggable-sidebar').addClass( "oculto", function() {
        });
        $(this).parent('#toggable-sidebar').removeClass( "scrol", function() {
        });
        $(this).hide();
        $('.show-menu').show();
    });
});