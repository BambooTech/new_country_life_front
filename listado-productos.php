<!doctype html>
<html class="no-js" lang="">
    
    <?php include 'common/head.php'; ?>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="wrapper" class="home">

            <?php include 'common/header.php'; ?>


            <div class="container">
                <div class="row">
                    
                    <?php include 'common/full-sidebar.php'; ?>

                    <div class="col-xs-12 col-sm-9">
                        <section id="show-products-grid">
                            <div class="row"> 
                                <div class="col-xs-12 section-title">
                                    <p>
                                        Productos destacados
                                    </p>
                                </div>
                                <div class="col-xs-4 col-sm-3 grid-item">
                                    <a href="#">
                                        <img src="http://placehold.it/195x195" class="img-responsive">
                                        <p class="product-name">
                                            Juego de toalla y toallón
                                        </p>
                                        <p class="product-brand">
                                            ZERO TWIST - KARSTEN 
                                        </p>
                                    </a>
                                </div>
                                
                                <div class="col-xs-4 col-sm-3 grid-item">
                                    <a href="#">
                                        <img src="http://placehold.it/195x195" class="img-responsive">
                                        <p class="product-name">
                                            Juego de toalla y toallón
                                        </p>
                                        <p class="product-brand">
                                            ZERO TWIST - KARSTEN 
                                        </p>
                                    </a>
                                </div>

                                <div class="col-xs-4 col-sm-3 grid-item">
                                    <a href="#">
                                        <img src="http://placehold.it/195x195" class="img-responsive">
                                        <p class="product-name">
                                            Juego de toalla y toallón
                                        </p>
                                        <p class="product-brand">
                                            ZERO TWIST - KARSTEN 
                                        </p>
                                    </a>
                                </div>

                                <div class="col-xs-4 col-sm-3 grid-item">
                                    <a href="#">
                                        <img src="http://placehold.it/195x195" class="img-responsive">
                                        <p class="product-name">
                                            Juego de toalla y toallón
                                        </p>
                                        <p class="product-brand">
                                            ZERO TWIST - KARSTEN 
                                        </p>
                                    </a>
                                </div>

                                <div class="col-xs-4 col-sm-3 grid-item">
                                    <a href="#">
                                        <img src="http://placehold.it/195x195" class="img-responsive">
                                        <p class="product-name">
                                            Juego de toalla y toallón
                                        </p>
                                        <p class="product-brand">
                                            ZERO TWIST - KARSTEN 
                                        </p>
                                    </a>
                                </div>

                                <div class="col-xs-4 col-sm-3 grid-item">
                                    <a href="#">
                                        <img src="http://placehold.it/195x195" class="img-responsive">
                                        <p class="product-name">
                                            Juego de toalla y toallón
                                        </p>
                                        <p class="product-brand">
                                            ZERO TWIST - KARSTEN 
                                        </p>
                                    </a>
                                </div>

                                <div class="col-xs-4 col-sm-3 grid-item">
                                    <a href="#">
                                        <img src="http://placehold.it/195x195" class="img-responsive">
                                        <p class="product-name">
                                            Juego de toalla y toallón
                                        </p>
                                        <p class="product-brand">
                                            ZERO TWIST - KARSTEN 
                                        </p>
                                    </a>
                                </div>

                                <div class="col-xs-4 col-sm-3 grid-item">
                                    <a href="#">
                                        <img src="http://placehold.it/195x195" class="img-responsive">
                                        <p class="product-name">
                                            Juego de toalla y toallón
                                        </p>
                                        <p class="product-brand">
                                            ZERO TWIST - KARSTEN 
                                        </p>
                                    </a>
                                </div>

                                <div class="col-xs-4 col-sm-3 grid-item">
                                    <a href="#">
                                        <img src="http://placehold.it/195x195" class="img-responsive">
                                        <p class="product-name">
                                            Juego de toalla y toallón
                                        </p>
                                        <p class="product-brand">
                                            ZERO TWIST - KARSTEN 
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>

            <?php include 'common/footer.php'; ?>
        </div>
    </body>
</html>
