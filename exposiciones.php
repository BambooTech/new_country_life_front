<!doctype html>
<html class="no-js" lang="">
    
    <?php include 'common/head.php'; ?>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="wrapper" class="home">

            <?php include 'common/header.php'; ?>


            <div class="container">
                <div class="row">
                    
                    <?php include 'common/full-sidebar.php'; ?>

                    <div class="col-xs-12 col-sm-9">
                        <section id="fullwidth-content">
                            <div class="row">
                                <div class="col-xs-12 section-title">
                                    <p>
                                        Título de la Exposición
                                    </p>
                                </div>
                                <div class="col-xs-12">
                                    <img src="img/demos/slider1.jpg" class="img-responsive">
                                    <p>
                                        <span>NEW COUNTRY LIFE</span> ha diseñado un concepto integrado de empresa joven y a la vez con una gran experiencia. Nuestra misión: “Ser los mas confiables, tanto para nuestros proveedores de Brasil, como para nuestros clientes en el mercado Argentino
                                    </p>
                                    <a href="#" class="btn btn-light">Ver imágenes</a>
                                </div>
                            </div>
                        </section>

                        <section id="fullwidth-content">
                            <div class="row">
                                <div class="col-xs-12 section-title">
                                    <p>
                                        Título de la Exposición
                                    </p>
                                </div>
                                <div class="col-xs-12">
                                    <img src="img/demos/slider2.jpg" class="img-responsive">
                                    <p>
                                        <span>NEW COUNTRY LIFE</span> ha diseñado un concepto integrado de empresa joven y a la vez con una gran experiencia. Nuestra misión: “Ser los mas confiables, tanto para nuestros proveedores de Brasil, como para nuestros clientes en el mercado Argentino
                                    </p>
                                    <a href="#" class="btn btn-light">Ver imágenes</a>
                                </div>
                            </div>
                        </section>

                        <section id="fullwidth-content">
                            <div class="row">
                                <div class="col-xs-12 section-title">
                                    <p>
                                        Título de la Exposición
                                    </p>
                                </div>
                                <div class="col-xs-12">
                                    <img src="img/demos/slider3.jpg" class="img-responsive">
                                    <p>
                                        <span>NEW COUNTRY LIFE</span> ha diseñado un concepto integrado de empresa joven y a la vez con una gran experiencia. Nuestra misión: “Ser los mas confiables, tanto para nuestros proveedores de Brasil, como para nuestros clientes en el mercado Argentino
                                    </p>
                                    <a href="#" class="btn btn-light">Ver imágenes</a>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>

            <?php include 'common/footer.php'; ?>
        </div>
    </body>
</html>
