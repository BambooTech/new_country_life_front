<!doctype html>
<html class="no-js" lang="">
    
    <?php include 'common/head.php'; ?>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="wrapper" class="home">

            <?php include 'common/header.php'; ?>


            <div class="container">
                <div class="row">
                    
                    <?php include 'common/full-sidebar.php'; ?>

                    <div class="col-xs-12 col-sm-9">
                        <section id="fullwidth-product-show">
                            <div class="row">
                                <div class="col-xs-12 section-title">
                                    <p>
                                        Product name
                                    </p>
                                </div>
                                <div class="col-xs-12 col-sm-7 fulwidth-product-main-img">
                                    <a href="#">
                                        <img src="img/demos/product-desc-tool.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <div class="col-xs-12 col-sm-5">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4">
                                            <div class="fullwidth-product-gallery">
                                                <a href="#">
                                                    <img src="img/demos/product-thumb-tool1.jpg" class="img-responsive">
                                                </a>
                                                <a href="#">
                                                    <img src="img/demos/product-thumb-tool1.jpg" class="img-responsive">
                                                </a>
                                                <a href="#">
                                                    <img src="img/demos/product-thumb-tool1.jpg" class="img-responsive">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-8">
                                            <div class="fullwidth-product-info">
                                                <h5>
                                                    Descripción del Producto
                                                </h5>
                                                <a href="#">
                                                    <img src="img/demos/slidertouch2.jpg" class="img-responsive">
                                                </a>
                                                <p class="fullwidth-product-detail">
                                                    Tela: 100% Algodón - 330 g/m²
                                                    <br>
                                                    Unidades de venta: 4   
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section id="fullwidth-product-preferences-tools">
                            <form>
                                <div class="col-xs-12">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="text-center">
                                        <tbody>
                                            <tr class="labels">
                                                <td><label>Seleccionar</label></td>
                                                <td><label>Código</label></td>
                                                <td><label>Tamaño</label></td>
                                                <td><label>Dimensiones (mm)</label><br>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td><label>A</label></td>
                                                                <td><label>B</label></td>
                                                                <td><label>C</label></td>
                                                                <td><label>D</label></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td><label>Peso sin Embalaje (kg)</label></td>
                                                <td><label>Cantidad</label></td>
                                                <td><label>Valor Unitario</label></td>
                                            </tr>
                                            
                                            <tr class="tools-item">
                                                <td>
                                                    <div class="checkbox">
                                                        <label>
                                                          <input type="checkbox" value="">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <p>123456</p>
                                                </td>
                                                <td>
                                                    <p>12 Dientes</p>
                                                </td>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td><p>10</p></td>
                                                                <td><p>20</p></td>
                                                                <td><p>30</p></td>
                                                                <td><p>40</p></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td>
                                                    <p>123</p>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <p class="unit">$1234</p> <small>small</small>
                                                </td>
                                            </tr>
                                            <tr class="tools-item">
                                                <td>
                                                    <div class="checkbox">
                                                        <label>
                                                          <input type="checkbox" value="">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <p>123456</p>
                                                </td>
                                                <td>
                                                    <p>12 Dientes</p>
                                                </td>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td><p>10</p></td>
                                                                <td><p>20</p></td>
                                                                <td><p>30</p></td>
                                                                <td><p>40</p></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td>
                                                    <p>123</p>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <p class="unit">$1234</p> <small>small</small>
                                                </td>
                                            </tr>
                                            <tr class="tools-item">
                                                <td>
                                                    <div class="checkbox">
                                                        <label>
                                                          <input type="checkbox" value="">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <p>123456</p>
                                                </td>
                                                <td>
                                                    <p>12 Dientes</p>
                                                </td>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td><p>10</p></td>
                                                                <td><p>20</p></td>
                                                                <td><p>30</p></td>
                                                                <td><p>40</p></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td>
                                                    <p>123</p>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <p class="unit">$1234</p> <small>small</small>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-xs-12 text-center">
                                    <button type="submit" class="btn btn-green">Agregar al Carrito</button>
                                </div>
                            </form>
                        </section>

                        <?php include 'common/section-grid.php'; ?>
                        
                    </div>
                </div>
            </div>

            <?php include 'common/footer.php'; ?>
        </div>
    </body>
</html>
