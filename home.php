<!doctype html>
<html class="no-js" lang="">
    
    <?php include 'common/head.php'; ?>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="wrapper" class="home">

            <?php include 'common/header.php'; ?>


            <div class="container">
                <div class="row">
                    
                    <?php include 'common/full-sidebar.php'; ?>

                    <div class="col-xs-12 col-lg-9">
                        
                        <?php include 'common/section-carousel.php'; ?>

                        <section id="touch-slider">
                            <!-- Set up your HTML -->
                            <div class="owl-carousel owl-theme owl-loaded">
                                <div>
                                    <img src="img/demos/slidertouch1.jpg">
                                </div>
                                <div>
                                    <img src="img/demos/slidertouch2.jpg">
                                </div>
                                <div>
                                    <img src="img/demos/slidertouch3.jpg">
                                </div>
                                <div>
                                    <img src="img/demos/slidertouch1.jpg">
                                </div>
                                <div>
                                    <img src="img/demos/slidertouch2.jpg">
                                </div>
                                <div>
                                    <img src="img/demos/slidertouch3.jpg">
                                </div>
                                <div>
                                    <img src="img/demos/slidertouch1.jpg">
                                </div>
                                <div>
                                    <img src="img/demos/slidertouch2.jpg">
                                </div>
                                <div>
                                    <img src="img/demos/slidertouch3.jpg">
                                </div>
                                <div>
                                    <img src="img/demos/slidertouch1.jpg">
                                </div>
                                <div>
                                    <img src="img/demos/slidertouch2.jpg">
                                </div>
                                <div>
                                    <img src="img/demos/slidertouch3.jpg">
                                </div>
                            </div>
                        </section>

                        <section class="readable">
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-sm-offset-2 text-center">
                                    <h4>
                                        NEW COUNTRY LIFE ha diseñado un concepto integrado de empresa joven y a la vez con una gran experiencia.
                                    </h4>
                                    <p>
                                        <strong>
                                            Nuestra misión
                                        </strong>
                                        <br>
                                         "Ser los mas confiables, tanto para nuestros proveedores de Brasil, como para nuestros clientes en el mercado argentino."
                                    </p>
                                </div>
                            </div>
                        </section>

                        <?php include 'common/section-grid.php'; ?>
                        
                    </div>
                </div>
            </div>

            <?php include 'common/footer.php'; ?>
        </div>
    </body>
</html>
