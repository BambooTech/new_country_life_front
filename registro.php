<!doctype html>
<html class="no-js" lang="">
    
    <?php include 'common/head.php'; ?>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="wrapper" class="home">

            <?php include 'common/header.php'; ?>


            <div class="container">
                <div class="row">
                    
                    <?php include 'common/full-sidebar.php'; ?>

                    <div class="col-xs-12 col-sm-9">
                        <section id="twocol-form">
                            <div class="row">
                                <div class="col-xs-12 section-title text-center">
                                    <p>
                                        Formulario de registro
                                    </p>
                                    <span>
                                        Complete sus datos para crear su cuenta y empiece a comprar online.
                                    </span>
                                </div>
                                <form>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="nombre">Nombre</label>
                                            <input type="text" class="form-control" id="nombre" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="apellido">Apellido</label>
                                            <input type="text" class="form-control" id="apellido" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" class="form-control" id="email" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="tel">Teléfono</label>
                                            <input type="text" class="form-control" id="tel" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="empresa">Empresa</label>
                                            <input type="text" class="form-control" id="empresa" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="cuit">Cuit</label>
                                            <input type="text" class="form-control" id="cuit" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="iva">Condición de IVA</label>
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="direccion">Dirección</label>
                                            <input type="text" class="form-control" id="direccion" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <p class="form-section">
                                            Datos del Transporte:
                                        </p>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="transporte-nombre">Nombre del transporte</label>
                                            <input type="text" class="form-control" id="transporte-nombre" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="transporte-direccion">Dirección</label>
                                            <input type="text" class="form-control" id="transporte-direccion" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="transporte-tel">Teléfono</label>
                                            <input type="text" class="form-control" id="transporte-tel" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="transporte-localidad">Localidad</label>
                                            <input type="text" class="form-control" id="transporte-localidad" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 text-right">
                                          <button type="submit" class="btn btn-green">Registrarme</button>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                </div>
            </div>

            <?php include 'common/footer.php'; ?>
        </div>
    </body>
</html>
