                    <section id="my-resume-confirm">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="my-resume-inner">
                                        <ul>
                                            <li>
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <p>
                                                            product name <span>(x3)</span>
                                                        </p>
                                                    </div>
                                                    <div class="col-xs-3 text-right">
                                                        <p>
                                                            $ 1234
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <p>
                                                            product name <span>(x3)</span>
                                                        </p>
                                                    </div>
                                                    <div class="col-xs-3 text-right">
                                                        <p>
                                                            $ 1234
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <p>
                                                            product name <span>(x3)</span>
                                                        </p>
                                                    </div>
                                                    <div class="col-xs-3 text-right">
                                                        <p>
                                                            $ 1234
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <p>
                                                            product name <span>(x3)</span>
                                                        </p>
                                                    </div>
                                                    <div class="col-xs-3 text-right">
                                                        <p>
                                                            $ 1234
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <p>
                                                            product name <span>(x3)</span>
                                                        </p>
                                                    </div>
                                                    <div class="col-xs-3 text-right">
                                                        <p>
                                                            $ 1234
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="total">
                                            <div class="row">
                                                <div class="col-xs-5">
                                                    <h5>
                                                        TOTAL: $
                                                    </h5>
                                                </div>
                                                <div class="col-xs-7 text-right">
                                                    <p>
                                                        580.-
                                                    </p>
                                                </div>
                                                <div class="col-xs-12 text-center">
                                                    <button type="submit" class="btn btn-green">FINALIZAR PEDIDO</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>