            <header>
                <div class="container-fluid header-bg">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-3">
                                <p class="show-menu">
                                    <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                </p>
                            </div>
                            <div class="col-xs-9 user-btns">
                                <a href="login.php" class="btn btn-nobg">ingresar</a>
                                <a href="registro.php" class="btn btn-cream">registrarse</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    
                    <section class="header-cols">
                        <div class="row header-row">
                            <div class="col-xs-6 col-lg-3 header-col">
                                <a href="#" class="logo">
                                    <img src="img/common/header/header-logo.jpg" class="img-responsive">
                                </a>
                            </div>

                            <div class="col-xs-6 col-lg-9 header-col inner">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="search-box">
                                            <form class="navbar-form navbar-right" role="search">
                                              <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Search">
                                                <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
                                              </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="main-nav">
                                            <nav class="navbar">
                                                <div class="container-fluid">
                                                <!-- Brand and toggle get grouped for better mobile display -->
                                                    <div class="navbar-header">
                                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                                            <span class="sr-only">Toggle navigation</span>
                                                            <i class="fa fa-bars" aria-hidden="true"></i>
                                                        </button>
                                                    </div>

                                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                                        <ul class="nav navbar-nav navbar-center">
                                                            <li><a href="home.php">home</a></li>
                                                            <li><a href="exposiciones.php">exposiciones</a></li>
                                                            <li><a href="como-comprar.php">como comprar</a></li>
                                                            <li><a href="showroom.php">showroom movil</a></li>
                                                            <li><a href="contacto.php">contacto</a></li>
                                                            <li><a href="carrito-checkout.php"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
                                                        </ul>
                                                    </div><!-- /.navbar-collapse -->
                                                </div><!-- /.container-fluid -->
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </header>