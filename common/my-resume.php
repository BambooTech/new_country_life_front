                        <section id="my-resume">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="my-resume-inner">
                                        <h5>
                                            MI PEDIDO
                                        </h5>
                                        <ul>
                                            <li>
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <a href="#"><i class="fa fa-times" aria-hidden="true"></i></a> <span>product name</span>
                                                    </div>
                                                    <div class="col-xs-3 text-right">
                                                        <span class="quantity">1</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <a href="#"><i class="fa fa-times" aria-hidden="true"></i></a> <span>product name</span>
                                                    </div>
                                                    <div class="col-xs-3 text-right">
                                                        <span class="quantity">10</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <a href="#"><i class="fa fa-times" aria-hidden="true"></i></a> <span>product name</span>
                                                    </div>
                                                    <div class="col-xs-3 text-right">
                                                        <span class="quantity">100</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <a href="#"><i class="fa fa-times" aria-hidden="true"></i></a> <span>product name</span>
                                                    </div>
                                                    <div class="col-xs-3 text-right">
                                                        <span class="quantity">1000</span>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="total">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <h5>
                                                        TOTAL: $
                                                    </h5>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <p>
                                                        580.-
                                                    </p>
                                                </div>
                                                <div class="col-xs-12 text-center">
                                                    <button type="submit" class="btn btn-green">FINALIZAR PEDIDO</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>