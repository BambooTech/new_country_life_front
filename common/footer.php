            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <p class="copyright">
                                Todos los derechos reservados a New Country Life - 2016
                            </p>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <p class="social">
                                Descubrí más sobre nosotros en: <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a> <a href=""><i class="fa fa-pinterest" aria-hidden="true"></i></a> <a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>